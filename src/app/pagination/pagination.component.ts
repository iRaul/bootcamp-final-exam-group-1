import { Component, OnInit,Input } from '@angular/core';
import { ProductService } from '../services/product.service'
import { first } from 'rxjs/operators'
import { Product} from '../model/product';
import { Router } from '@angular/router';
import { ProductComponent } from '../product/product.component';
@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() pageNumber:number;
  products:Product[] = [];
  totalItems:number=10;
  totalOfBtn:any;
  brandPagination:any;
  newProducts:Product[]=[]
  brandName:any;
  constructor(
    private router:Router,
    private productService : ProductService,
  ) { }

  ngOnInit(): void {
    this.brandName=localStorage.getItem("menu-brand");
    this.productService.searchProduct("").pipe(first()).subscribe((data:any)=>{
      this.products=data;
      this.newProducts = this.products.filter(res=>{
        return res.brand.toLocaleLowerCase().match(this.brandName.toLocaleLowerCase())
      })
      
     this.totalOfBtn=this.get();
    })
  
  
  }
  get(){
    console.log(this.newProducts.length,"LN")
    return Math.round(this.products.length/this.totalItems)
  }
  counter(i :any){
    return new Array(i);
  }

  nextPage(id:number){
    this.productService.pagination(id).pipe(first()).subscribe(data=>{
      console.log(data);
    })
    // this.pageNumber=id
    // this.productComp.loadPage(id)
    // this.router.navigate(['page/',this.pageNumber]);    
  }

}
