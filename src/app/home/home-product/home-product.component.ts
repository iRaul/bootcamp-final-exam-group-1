import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Product} from '../../model/product';
import { ProductService } from '../../services/product.service'
import { DataService } from '../../data.service';

@Component({
  selector: 'app-home-product',
  templateUrl: './home-product.component.html',
  styleUrls: ['./home-product.component.scss']
})
export class HomeProductComponent implements OnInit {
  products:Product[]=[]
  pageNumber:number=1;
  constructor(  
    private dataService: DataService,
    private productService: ProductService,
    private router:Router) { }

  ngOnInit(): void {
    this.loadPage(this.pageNumber)
  }
  loadPage(pageNum:number){
    this.productService.pagination(pageNum).pipe(first()).subscribe((data:any)=>{
      this.products=data
    });
  }
  viewProduct(id:any,brand:any,name:any,category:any){
    this.dataService.getProductById(id).pipe(first()).subscribe();
    localStorage.setItem("prod-id",id)
    localStorage.setItem('menu-brand',brand);
    localStorage.setItem('menu-title','Product')
    localStorage.setItem('menu-category',category)
    localStorage.setItem('menu-prod-name',name)
    this.router.navigate(['product/'+category+'/'+brand+'/'+id+'/'+name]).then(()=>{
      window.location.reload();
    })
  }  
}
