import { Component, OnInit } from '@angular/core';
import { LocationStrategy,PlatformLocation } from '@angular/common';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { DataService } from '../data.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userName: any;
  userID: any;
  loginStatus:any;
  constructor(
    private router: Router,
    private dataService: DataService
  ) { }

  ngOnInit(): void {
    Swal.fire({ 
      background:'transparent',
      timer: 500
    });
    Swal.showLoading()
    localStorage.setItem('cart-checkout',"false")
    localStorage.removeItem('menu-title')
    localStorage.removeItem('menu-brand')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    localStorage.removeItem("prod-id")
    localStorage.removeItem('checkout-total-item')
    localStorage.removeItem('checkout-total-amount')
    localStorage.removeItem('checkout')
    localStorage.removeItem('checkout-cart-id')

    this.checkHome()
    this.userName = localStorage.getItem('username');
    this.userID = localStorage.getItem('userid');
    this.loginStatus = localStorage.getItem("loginStatus")
  }

  checkHome(){
    if(localStorage.getItem("loginStatus") != null){
      ;
    }else{
      this.router.navigate(['']).then(()=>{
        window.location.reload();
      })
    }
  }
 

}
