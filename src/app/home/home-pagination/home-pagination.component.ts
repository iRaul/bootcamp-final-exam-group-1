import { Component, OnInit,Input } from '@angular/core';
import { ProductService } from '../../services/product.service'
import { first } from 'rxjs/operators'
import { Product} from '../../model/product';
import { Router } from '@angular/router';
import { HomeProductComponent } from '../home-product/home-product.component'
@Component({
  selector: 'app-home-pagination',
  templateUrl: './home-pagination.component.html',
  styleUrls: ['./home-pagination.component.scss']
})
export class HomePaginationComponent implements OnInit {
  products:Product[] = [];
  @Input() pageNumber:number;
  totalItems:number=10;
  totalOfBtn:any;
  constructor(
    private homeProduct: HomeProductComponent,
    private router:Router,
    private productService : ProductService,
  ) { }

  ngOnInit(): void {
    this.productService.searchProduct("").pipe(first()).subscribe((data:any)=>{
      this.products=data;

     this.totalOfBtn=this.get();
    })
  
  }
  get(){
    return Math.round(this.products.length/this.totalItems)
  }
  counter(i :any){
    return new Array(i);
  }

  nextPage(id:number){
    this.productService.pagination(id).pipe(first()).subscribe(data=>{
     
    })
    this.pageNumber=id;
    this.homeProduct.loadPage(this.pageNumber)
    this.router.navigate(['home/',this.pageNumber]);    
  }

}
