import { Component, OnInit } from '@angular/core';
import { ImagePath } from '../model/image-path';
import { Product } from '../model/product'
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})

export class LandingPageComponent implements OnInit {
 
  constructor(
   private router:Router
  ) { }

  ngOnInit(): void {
    Swal.fire({ 
      showConfirmButton: false,
      background: 'transparent',  
      timer: 1000
    });
    Swal.showLoading()
    if(localStorage.getItem("loginStatus") == "active" ){
      this.router.navigate(['home'])
    }else{
      this.router.navigate([''])
    }
    localStorage.setItem('cart-checkout',"false")
    localStorage.removeItem('menu-title')
    localStorage.removeItem('menu-brand')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    localStorage.removeItem("prod-id")
  }
  public slides: ImagePath[] = [
    {
      src:"../assets/img/product/laptop/banner/laptop1.jpg",
      name:''
    },
    {
      src:"../assets/img/product/laptop/banner/laptop2.jpg",
      name:''
    },
    {
      src:"../assets/img/product/laptop/banner/laptop3.jpg",
      name:''
    },
    {
      src:"../assets/img/product/laptop/banner/laptop4.jpg",
      name:''
    },
    {
      src:"../assets/img/product/laptop/banner/laptop5.jpg",
      name:''
    }
  ]
  public categories: ImagePath[] = [
    {
      src:"../assets/img/category/laptop.png",
      name:'Laptop'
    },
    {
      src:"../assets/img/category/desktop.png",
      name:'Desktop'
    },
    {
      src:"../assets/img/category/monitor.png",
      name:'Monitor'
    },
    {
      src:"../assets/img/category/mousekey.png",
      name:'Peripherals'
    },
    
  ]
}
