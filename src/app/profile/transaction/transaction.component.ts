import { Component, OnInit } from '@angular/core';
import jspdf from 'jspdf';
import Swal from 'sweetalert2'
import { DataService } from '../../data.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import 'jspdf-autotable';
@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  transaction:any=[]=[]
  userID:any;
  name:any;
  userName:any;
  constructor(
    private dataService: DataService,
    private router:Router
  ) { }
  head = [['ID', 'Country', 'Rank', 'Capital']]
  
  ngOnInit(): void {
    this.userID=localStorage.getItem('userid')
    this.name=localStorage.getItem('full_name')
    this.userName = localStorage.getItem('username')
    this.dataService.getOrder(this.userID).pipe(first()).subscribe((data:any=[])=>{
      this.transaction=data;
      console.log(this.transaction,"data")
    })
    
  } 
  goToProfile(){
    localStorage.setItem('cart-checkout',"false")
    Swal.fire({ 
      background:'transparent',
      timer: 500
    }).then(()=>{
      this.router.navigate(['user/profile']).then(()=>{
        window.location.reload()
      })
    });
    Swal.showLoading()
  } 
  logout(){
    Swal.fire({ 
      icon: 'success',  
      title: 'OL Shoppee',  
      text: 'Please Wait..',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.clear();
      this.router.navigate(['']).then(() => {
        window.location.reload();
      });
  });
  }
  goToHome(){
    this.router.navigate(['home']).then(()=>{
      window.location.reload()
    })
  }
  createPdf() {
   
    let doc = new jspdf('p', 'mm', 'a4');

    doc.setFontSize(18);
    doc.text(this.name+' Transaction History', 11, 8);
    doc.setFontSize(11);
    doc.setTextColor(100);


    (doc as any).autoTable({
    
      body: this.transaction,
      theme: 'plain',
      didDrawCell: (data:any) => {
        console.log(data.column.index)
      }
    })

    // Open PDF document in new tab
    doc.output('dataurlnewwindow')

    // Download PDF document  
    doc.save(this.name+'.pdf');
  }

}
