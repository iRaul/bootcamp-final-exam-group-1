import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2'
import { DataService } from '../data.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  constructor(
    private router:Router
  ) { }

  ngOnInit(): void {
    
  }

  goToTransaction(){
    Swal.fire({ 
      background:'transparent',
      timer: 1000
    }).then(()=>{
      this.router.navigate(['user/transaction']).then(()=>{
        localStorage.setItem("cart-checkout","true")
        window.location.reload()
      })
    });
    Swal.showLoading()
    
  }
}
