import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { first } from 'rxjs/operators'
import { Product} from '../model/product';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  products:Product[] = [];
  isShowItemCart=false;
  title = 'Online Shop';  
  isLogin:any;
  isShowNav:boolean=true;
  isShowUser:boolean=false;
  name:String="";//USE TO SEARCH PRODUCT
  showResult:boolean=false;
  cart:any[]=[]
  brandList:string[]=[]
  newBrandList:string[]=[]
  userID:any;
  cartLength:any;
  prodctLength:number=0;
  userName:any;

  menuName:any;
  menuTitle:any;
  menuShow:boolean=false;
  prodName:any;
  menuCategory:any;
  showMenuBrand:boolean=false;
  showProdName:boolean=false;
  showCategory:boolean=false;

  cartCheckout:boolean=true;
  constructor(
    private router: Router,
    private cartService: CartService,
    private productService : ProductService,
    private dataService: DataService) { }

  ngOnInit(): void {
    //SETTING FOR CATEGORY
    this.prodName = localStorage.getItem("menu-prod-name")?.toUpperCase();
    this.menuTitle = localStorage.getItem("menu-title")?.toUpperCase();
    this.menuName = localStorage.getItem("menu-brand")?.toUpperCase();
    this.menuCategory = localStorage.getItem('menu-category')?.toUpperCase();
    if(localStorage.getItem('cart-checkout') == "false"){
      this.cartCheckout=false;
    }
    if(this.menuName != null){
      this.showMenuBrand=true;
    }
    if(this.prodName != null){
      this.showProdName=true;
    }
    if(this.menuCategory != null){
      this.showCategory=true;
    }
    if(this.menuTitle != null && this.menuTitle != null ){
      this.menuShow=true;
    }else{
      this.menuShow=false;
    }
    // FOR GETTING CART AFTER SUCCESSFULL LOGIN
    this.userName = localStorage.getItem('username')
    this.userID = localStorage.getItem('userid')
    if(this.userID != null){
      this.cartService.getCart(this.userID).pipe(first()).subscribe((data:any)=>{
        this.cart=data.Data.Cart;
        this.cartLength=this.cart.length ;
      })
    }


    this.showResult=false;// FOR SHOWING OF NAVBAR LOGIN REGISTER
    this.isLogin = localStorage.getItem('loginStatus')
    if(this.isLogin === "active"){
      this.isShowNav=false;
      this.isShowUser=true;
      this.isShowItemCart=true;
    }
    //GET ALL BRAND OF BRANCH BUT UNIQUE WITH PUT ALL PRODUCT IN this.products.
    this.productService.searchProduct(this.name).pipe(first()).subscribe((data:any)=>{
      this.products=data;
      this.prodctLength=this.products.length
      for(let i = 0 ; i < this.prodctLength ; i++){
        this.brandList.push(this.products[i].brand)
      }
    })
    
  }
  //LOOP TO REMOVE DUPLICATE VALUES IN BRANCH.
  getUnique(array:string[]=[]){
    var uniqueArray = [];
    
    // Loop through array values
    for(let i=0; i < array.length; i++){
        if(uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    return uniqueArray;
  }
  //TO HIDE RESULT OF SEARCH
  gotoLogin(){
    localStorage.clear();
    
    this.router.navigate(['login']).then(()=>{
    
      window.location.reload();
    })
    this.showResult=false;
  }
  gotoRegistration(){
    localStorage.clear();
   
    this.router.navigate(['register']).then(()=>{
      localStorage.setItem("cart-checkout","true")
      window.location.reload();
    })
    this.showResult=false; 
  }
  //GO TO USER PROFLE..
  goToProfile(){
    localStorage.setItem('cart-checkout',"false")
    Swal.fire({ 
      background:'transparent',
      timer: 500
    }).then(()=>{
      this.router.navigate(['user/profile']).then(()=>{
        window.location.reload()
      })
    });
    Swal.showLoading()
  } 
  //GO TO USER CART
  gotoCart(){
    if(this.userID == null && this.userName == null){
      Swal.fire({ 
        icon: 'error',  
        title: 'Authentication Required',  
        text: 'Please Login First To Access Cart.',
        showConfirmButton: true,  
        timer: 1000
      });
      Swal.showLoading()
    } else {
      Swal.fire({ 
        icon: 'success',  
        title: 'OL Shoppee',  
        text: 'One Moment Please...',
        showConfirmButton: false,  
        timer: 1000
      }).then(()=>{
        this.router.navigate(['cart']).then(() => {
          window.location.reload();
        });
      })
      Swal.showLoading()
    }
  }
  //SEARCH PRODUCT ON CHANGE EVENT IN INPUT
  searchProduct(){
    if(this.name == ""){
      this.ngOnInit();
      this.showResult=false;
    }else {
      if(this.products.length == 0){
        this.showResult=false;
      } else {
        this.products = this.products.filter(res=>{
          return res.productName.toLocaleLowerCase().match(this.name.toLocaleLowerCase())
        })
        this.showResult=true;
      }
      
   }
  
  }
  //CLICK ONE PRODOCUT TO VIEW
  viewProduct(id:any){
      this.showResult=false;
      var prodName;
      this.dataService.getProductById(id).pipe(first()).subscribe((data:any=[])=>{
        localStorage.setItem('menu-title',"Product")
        localStorage.setItem('menu-brand',data.brand)
        localStorage.setItem('menu-category',data.category)
        localStorage.setItem("menu-prod-name",data.productName)
        localStorage.setItem("prod-id",id)
        this.router.navigate(['product/'+data.category+'/'+data.brand+'/'+id+'/'+data.productName]).then(()=>{
        window.location.reload();
      })
      });
      
    }  
    //LOGOUT 
  logout(){
    Swal.fire({ 
      icon: 'success',  
      title: 'OL Shoppee',  
      text: 'Please Wait..',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      this.isShowNav=true;
      this.isShowUser=false;
      this.isShowItemCart=false;
      localStorage.clear();
      this.router.navigate(['']).then(() => {
        window.location.reload();
      });
  });
  }

  goToHome(){//GO TO HOME METHOD
    if(localStorage.getItem('loginStatus') != null){
      localStorage.removeItem('menu-title');
      localStorage.removeItem('menu-name');
      localStorage.removeItem('menu-page')
      localStorage.removeItem('menu-prod-name')
      this.router.navigate(['home']).then(()=>{
        window.location.reload();
      })
    }else{
      localStorage.clear();
      this.router.navigate(['']).then(()=>{
        window.location.reload();
      })
    }
  }
  //VIEW CATEGORY USE IN NAVBAR HEADER
  goToCategory(name:string){
    localStorage.removeItem('menu-title');
    localStorage.removeItem('menu-brand');
    localStorage.removeItem('menu-page')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1500
    }).then(()=>{
      this.router.navigate(['product/category/'+name]).then(() => {
        localStorage.setItem('menu-title','Product')
        localStorage.setItem('menu-category',name);
        localStorage.setItem('prod-sorting','default')
        window.location.reload();
      });
    })
    Swal.showLoading()
  }    

  goToBrand(name:string){
    localStorage.removeItem('menu-title');
    localStorage.removeItem('menu-brand');
    localStorage.removeItem('menu-page')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1500
    }).then(()=>{
      this.router.navigate(['product/brand/'+name]).then(() => {
        localStorage.setItem('menu-title','Product')
        localStorage.setItem('menu-brand',name);
        localStorage.setItem('prod-sorting','default')
        window.location.reload();
         
      });
    })
    Swal.showLoading()
  } 
}

