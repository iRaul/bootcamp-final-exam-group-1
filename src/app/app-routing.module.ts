import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CartComponent } from './cart/cart.component';
import { CreditCardComponent } from './credit-card/credit-card.component';
import { HomeComponent } from './home/home.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { ProductViewComponent } from './product/product-view/product-view.component';
import { RegisterComponent } from './register/register.component';
import { NotFoundComponent } from '../app/not-found/not-found.component'
import { CategoryViewComponent } from './product/category-view/category-view.component';
import { BrandViewComponent } from './product/brand-view/brand-view.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProfileComponent } from './profile/profile.component';

import { TransactionComponent } from './profile/transaction/transaction.component';
import { AccountComponent } from './profile/account/account.component';
const routes: Routes = [
  {path: '',component:LandingPageComponent},
  {path: 'page/:id',component: LandingPageComponent},
  { path: 'login',component: LoginComponent},
  { path: 'register',component:RegisterComponent},
  { path: 'product/:category/:brand/:id/:name',component:ProductViewComponent},
  { path: 'cart',component:CartComponent},
  { path: 'add/credit',component:CreditCardComponent},
  { path: 'home',component:HomeComponent},
  { path: 'home/:id',component:HomeComponent},
  { path: 'product/category/:name',component:CategoryViewComponent},
  { path: 'product/brand/:name',component:BrandViewComponent},
  { path: 'product/cart/checkout',component:CheckoutComponent},
  { path: 'user/profile',component:ProfileComponent},
  { path: 'user/account',component:AccountComponent},
  { path: 'user/transaction',component:TransactionComponent},
  { path: '404', component:NotFoundComponent},
  { path: '**', redirectTo: '/404'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
