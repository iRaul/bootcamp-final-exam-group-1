import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/data.service';
import { first } from 'rxjs/operators'; 
import Swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  hidePassword = true;
  hideConfirmPassword = true;
  imagePath = "../../assets/img/off-eye.png";
  imagePath2 = "../../assets/img/off-eye.png";
  submitted=false;

  form: FormGroup;
  registerForm = this.formBuilder.group({
    name: '',
    username: '',
    password: '',
    email:'',
    address:'',
    contact_number:''
  });
  constructor(
    private router:Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    localStorage.setItem("cart-checkout","false")
    Swal.fire({ 
      showConfirmButton: false,
      background: 'transparent',  
      timer: 500
    });
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.pattern]],
      address: ['', Validators.required],
      contact_number: ['', Validators.required]
    });
  }
  showPassword (){
    if(this.hidePassword){
      this.imagePath="../../assets/img/on-eye.png";
      this.hidePassword=false;
     
    } else {
      this.imagePath="../../assets/img/off-eye.png";
      this.hidePassword=true;
    }
  }
  showConfirmPass (){
    if(this.hideConfirmPassword){
      this.imagePath2="../../assets/img/on-eye.png";
      this.hideConfirmPassword=false;
     
    } else {
      this.imagePath2="../../assets/img/off-eye.png";
      this.hideConfirmPassword=true;
    }
  }
  get f() { return this.registerForm.controls; }
  onSubmit() {
    this.submitted = true;
    // console.log("clicked")
    // console.log(this.f.name.errors)
    // if(this.f.name.errors || this.f.username.errors || this.f.password.errors || this.f.email.errors || this.f.email.errors || this.f.contact_number.errors){
    // }else{
      this.dataService.register(this.registerForm.value).pipe(first()).subscribe(data =>{
        Swal.fire({ 
          icon: 'success',  
          title: 'OL Shoppee',  
          text: 'User Registration Successfull! Please Login.',
          showConfirmButton: false,  
          timer: 3500
        }).then(()=>{
          this.router.navigate(['login']).then(() => {
            window.location.reload();
          });
        })
      })
    
  }
}
