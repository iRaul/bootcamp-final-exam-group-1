import { Component, OnInit,Input } from '@angular/core';
import { Product} from '../model/product';
import { DataService } from '../data.service'
import { first } from 'rxjs/operators';
import { ProductService } from '../services/product.service'
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {
  productID:any;
  laptops:any[]=[]
  desktop:any[]=[]
  monitor:any[]=[]
  peripherals:any[]=[]
  mobiles:any[]=[]
  p=0;
  pageNumber=1;
  products:Product[] = [];

  constructor(
  
    private dataService: DataService, 
    private productService: ProductService,
    private router:Router
  ) { }

  ngOnInit(): void {
    Swal.fire({ 
      showConfirmButton: false,
      background: 'transparent',  
      timer: 500
    });
    this.loadDesktop();
    this.loadLaptop();
    this.loadMonitor();
    this.loadPeripherals();
    this.loadMobile();
    this.loadPage(this.pageNumber)
    
  }
  goToCategory(name:string){
    localStorage.removeItem('menu-title');
    localStorage.removeItem('menu-brand');
    localStorage.removeItem('menu-page')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1500
    }).then(()=>{
      this.router.navigate(['product/category/'+name]).then(() => {
        localStorage.setItem('menu-title','Product')
        localStorage.setItem('menu-category',name);
        localStorage.setItem('prod-sorting','default')
        window.location.reload();
      });
    })
    Swal.showLoading()
  }    
  viewProduct(id:any,brand:any,name:any,category:any){
    this.dataService.getProductById(id).pipe(first()).subscribe();
    localStorage.setItem("prod-id",id)
    localStorage.setItem('menu-brand',brand);
    localStorage.setItem('menu-title','Product')
    localStorage.setItem('menu-category',category)
    localStorage.setItem('menu-prod-name',name)
    this.router.navigate(['product/'+category+'/'+brand+'/'+id+'/'+name]).then(()=>{
      window.location.reload();
    })
  }  
  loadLaptop(){
    this.productService.getAllProductByCategory("Laptop",1).pipe(first()).subscribe((data:any=[])=>{
      this.laptops=data;
    })
  }
  loadDesktop(){
    this.productService.getAllProductByCategory("Desktop",1).pipe(first()).subscribe((data:any=[])=>{
      this.desktop=data;
    })
  }
  loadMonitor(){
    this.productService.getAllProductByCategory("Monitor",1).pipe(first()).subscribe((data:any=[])=>{
      this.monitor=data;
    })
  }
  loadPeripherals(){
    this.productService.getAllProductByCategory("Peripherals",1).pipe(first()).subscribe((data:any=[])=>{
      this.peripherals=data;
    })
  }
  loadMobile(){
    this.productService.getAllProductByCategory("Mobile Phone",1).pipe(first()).subscribe((data:any=[])=>{
      this.mobiles=data;
    })
  }
  loadPage(pageNum:number){
    this.productService.pagination(pageNum).pipe(first()).subscribe((data:any)=>{
      this.products=data
    });
  }
}
