import { Component, OnInit } from '@angular/core';
import { Product} from '../../model/product';
import { ProductService } from '../../services/product.service'
import { first } from 'rxjs/operators'
import Swal from 'sweetalert2'
import { DataService } from 'src/app/data.service';
import { Router } from '@angular/router';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-brand-view',
  templateUrl: './brand-view.component.html',
  styleUrls: ['./brand-view.component.scss']
})
export class BrandViewComponent implements OnInit {
  products:Product[] = [];
  newProducts:Product[] = [];
  lowestProduct:Product[] =[];
  highestProduct:Product[]=[];
  tempProd:Product[]=[];
  menuName:any;
  sorting:any;
  p:number=1
  constructor(
    private dataService:DataService,
    private router:Router,
    private productService:ProductService
  ) { }
  ngOnInit(): void {
  
    this.sorting=localStorage.getItem("prod-sorting")
    this.menuName=localStorage.getItem("menu-brand");
    this.productService.searchProduct("").pipe(first()).subscribe((data:any=[])=>{
      this.products=data;
      switch(this.sorting) { 
        case "default": { 
            this.newProducts = this.products.filter(res=>{
              return res.brand.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
            })
            console.log(this.newProducts)
      
        break; 
        }
        case "lowest-highest": { 
          this.newProducts = this.products.filter(res=>{
            return res.brand.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
          })
          this.newProducts = this.newProducts.sort((a,b)=>parseInt(a.price) - parseInt(b.price));
          break;
        }
        case "highest-lowest": { 
          this.newProducts = this.products.filter(res=>{
            return res.brand.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
          })
          this.newProducts = this.newProducts.sort((a,b)=>parseInt(b.price) - parseInt(a.price));
          break;
        }
      }
    });

  }
  viewProduct(id:any,brand:any,name:any,category:any){
    this.dataService.getProductById(id).pipe(first()).subscribe();
    localStorage.setItem("prod-id",id)
    localStorage.setItem('menu-brand',brand);
    localStorage.setItem('menu-title','Product')
    localStorage.setItem('menu-category',category)
    localStorage.setItem('menu-prod-name',name)
    this.router.navigate(['product/'+category+'/'+brand+'/'+id+'/'+name]).then(()=>{
      window.location.reload();
    })
  } 
  lowestToHighest(){ 
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.setItem('prod-sorting','lowest-highest')
        window.location.reload();
    })
    Swal.showLoading()
  }
  highestToLowest(){
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.setItem('prod-sorting','highest-lowest')
        window.location.reload();
    })
    Swal.showLoading()
  }

}
