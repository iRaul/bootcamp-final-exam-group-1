import { Component, OnInit} from '@angular/core';
import { ProductService } from '../../services/product.service'
import { first } from 'rxjs/operators';
import { LocationStrategy,PlatformLocation } from '@angular/common';
import { Product } from '../../model/product'
import { Router } from '@angular/router';
import { DataService } from 'src/app/data.service'
import { HeaderComponent } from 'src/app/header/header.component';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.scss']
})
export class CategoryViewComponent implements OnInit {
  products:Product[] = [];
  newProducts:Product[] = [];
  lowestProduct:Product[] =[];
  highestProduct:Product[]=[];
  menuName:any;
  sorting:any;
  p:number=1
  constructor(
    private router:Router,
    private dataService:DataService,
    private productService:ProductService,
    private location: PlatformLocation

  ) { }

  ngOnInit(): void {
    
    this.sorting=localStorage.getItem("prod-sorting")
    this.menuName=localStorage.getItem("menu-category");
    this.productService.searchProduct("").pipe(first()).subscribe((data:any=[])=>{
      this.products=data;
      switch(this.sorting) { 
        case "default": { 
            this.newProducts = this.products.filter(res=>{
              return res.category.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
            })
            console.log(this.newProducts.length)
      
        break; 
        }
        case "lowest-highest": { 
          console.log(this.products)
          this.newProducts = this.products.filter(res=>{
            return res.category.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
          })
          this.newProducts = this.newProducts.sort((a,b)=>parseInt(a.price) - parseInt(b.price));
          break;
        }
        case "highest-lowest": { 
          console.log(this.products)
          this.newProducts = this.products.filter(res=>{
            return res.category.toLocaleLowerCase().match(this.menuName.toLocaleLowerCase())
          })
          this.newProducts = this.newProducts.sort((a,b)=>parseInt(b.price) - parseInt(a.price));
          break;
        }
      }
    });
    
   }
   viewProduct(id:any,brand:any,name:any,category:any){
    this.dataService.getProductById(id).pipe(first()).subscribe();
    localStorage.setItem("prod-id",id)
    localStorage.setItem('menu-brand',brand);
    localStorage.setItem('menu-title','Product')
    localStorage.setItem('menu-category',category)
    localStorage.setItem('menu-prod-name',name)
    this.router.navigate(['product/'+category+'/'+brand+'/'+id+'/'+name]).then(()=>{
      window.location.reload();
    })
  } 
   lowestToHighest(){ 
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.setItem('prod-sorting','lowest-highest')
        window.location.reload();
    })
    Swal.showLoading()
  }
  highestToLowest(){
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.setItem('prod-sorting','highest-lowest')
        window.location.reload();
    })
    Swal.showLoading()
  }

}
