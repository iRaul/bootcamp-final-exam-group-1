import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AddCart } from 'src/app/model/add-cart';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CartService } from 'src/app/services/cart.service';
import Swal from 'sweetalert2'
import { PlatformLocation } from '@angular/common';
@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {
  addCart:AddCart[]=[];
  productID:any;
  userName: any;
  userID: any;
  quantity:number = 1;
  usetStatus:any;
  productView:any=[]

  convertedUserID:number=0;
  convertedProdID:number=0;

  form: FormGroup;
  addCartForm = this.formBuilder.group({
    user_id: '',
    product_id: '',
    order_quantity: '',
  });


  constructor(
    private location: PlatformLocation,
    private dataService: DataService,
    private cartService: CartService,
    private formBuilder: FormBuilder,
  private router: Router) { }

  ngOnInit(): void {
    Swal.fire({ 
      showConfirmButton: false,
      background: 'transparent',  
      timer: 1000
    });
    Swal.showLoading()
    this.usetStatus = localStorage.getItem('loginStatus')
    this.userID = localStorage.getItem('userid')
    this.productID = localStorage.getItem('prod-id');
    
    this.dataService.getProductById(this.productID).pipe(first()).subscribe((data:any=[])=>{
        this.productView.push({
          id:data['id'],
          productName:data['productName'],
          category: data['category'],
          brand: data['brand'],
          productDescription: data['productDescription'],
          imagePath: data['imagePath'],
          productQuantity: data['productQuantity'],
          price:data['price']
        })

    });

    this.addCartForm = this.formBuilder.group({
      user_id: ['', Validators.required],
      product_id: ['', Validators.required],
      order_quantity: ['', Validators.required],
    });
   
  }

  
  addQty(){
    if(this.quantity != parseInt(this.productView[0].productQuantity)){
      this.quantity++;
    }
  }
  minusQty(){
    if(this.quantity !=1 ){
      this.quantity--;
    }
  }
  onSubmit(){
    console.log(this.quantity)
    this.addCartForm.patchValue({order_quantity:this.quantity})
    this.addCartForm.patchValue({user_id:parseInt(this.userID)})
    this.addCartForm.patchValue({product_id:parseInt(this.productID)})
    const cart={
      user_id:this.userID,
      product_id:this.productID,
      order_quantity:this.quantity
    }
    if(this.usetStatus == "active" && this.quantity>0){
      this.cartService.addToCart(cart).pipe(first()).subscribe(data=>{  
        Swal.fire({ 
          icon: 'success',  
          title: 'OL Shoppee',  
          text: this.productView[0].productName+" has been added to your cart..",
          showConfirmButton: false,  
          timer: 1000
        }).then(()=>{
          this.router.navigate(['cart']).then(() => {
            window.location.reload();
          });
        })
      });
    }
    else if(this.usetStatus == "active" && this.quantity==0){
      Swal.fire({ 
        icon: 'error',  
        title: 'OL Shoppee',  
        text: 'Invalid quantity',
        showConfirmButton: false,  
        timer: 1000
      })
      Swal.showLoading()
    } else {
      Swal.fire({ 
        icon: 'error',  
        title: 'Authentication Required',  
        text: 'Please Login First...',
        showConfirmButton: true,  
        timer: 1500
      }).then(()=>{
        this.router.navigate(['login']).then(() => {
          window.location.reload();
        });
      })
      Swal.showLoading()
    }
  }
}
