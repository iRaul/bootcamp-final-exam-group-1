import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private REST_API="http://45.130.229.44:8080/api/1.0.0/";
  // private REST_API="http://localhost:8080/api/1.0.0/";
  constructor(private httpClient: HttpClient, private route: Router) { }
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization' : 'Basic ' + btoa('admin:admin'),
  'Access-Control-Allow-Headers' : 'X-Requested-With,content-type'})};

  searchProduct(name:String){
    return this.httpClient.get(this.REST_API+`Product/Search?name=`+name, this.httpOptions);
  }
  pagination(id:number){ //GET 10 PRODUCTS WITH PAGINATION...
    return this.httpClient.get(this.REST_API+`Product/All/${id}`,this.httpOptions);
  }

  getAllProductByCategory(category:any,page:any){
    return this.httpClient.get(this.REST_API+`Product/Category/${category}/${page}`,this.httpOptions);
  }
}
