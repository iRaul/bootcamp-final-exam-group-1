import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router'
import { Account } from '../model/account';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

 private REST_API="http://45.130.229.44:8080/api/1.0.0/";
  // private REST_API="http://localhost:8080/api/1.0.0/";

 public account: Observable<Account>;
 constructor(private httpClient: HttpClient, private route: Router) { }
 httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json',
 'Accept': 'application/json',
 'Authorization' : 'Basic ' + btoa('admin:admin'),
 'Access-Control-Allow-Headers' : 'X-Requested-With,content-type'})};

 addAccount(user:Account){
  return this.httpClient.post(this.REST_API + "Account/Add",user, this.httpOptions);
  }
}
