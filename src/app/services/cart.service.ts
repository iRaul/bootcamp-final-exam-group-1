import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AddCart } from '../model/add-cart';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  public cart: Observable<AddCart>;
  delCart:any[];
  // private REST_API="http://localhost:8080/api/1.0.0/";
  private REST_API="http://45.130.229.44:8080/api/1.0.0/";
  constructor(private httpClient: HttpClient) { }
  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json',
  'Accept': 'application/json',
  'Authorization' : 'Basic ' + btoa('admin:admin'),
  'Access-Control-Allow-Headers' : 'X-Requested-With,content-type'})};

  addToCart(cart:AddCart){
    return this.httpClient.post(this.REST_API+"Cart/Product/Add",cart, this.httpOptions);
  }

  getCart(id:any){
    return this.httpClient.get(this.REST_API+`Cart/User/${id}`, this.httpOptions);
  }
  updateCart(cartID:any,orderQty:any){
    const cartUpd = {
      cart_id:cartID,
      order_quantity:orderQty
    }
    return this.httpClient.post(this.REST_API+"Cart/Product/Update",cartUpd,this.httpOptions)
  }
  deleteCart(id:any[]){
    const cartDel ={
      cart_id:id
    }
    console.log(id,"SERVICE ID DELET")
    return this.httpClient.post(this.REST_API+"Cart/Product/Delete",cartDel,this.httpOptions)
  }

  checkout(user_id:any,account_id:any){
    const checkout = {
      user_id:user_id,
      account_id:account_id
    }
    return this.httpClient.post(this.REST_API+"Cart/User/Checkout",checkout,this.httpOptions)
  }
  checkoutCOD(user_id:any){
    const checkout = {
      user_id:user_id,
      payment_option:"COD"
    }
    return this.httpClient.post(this.REST_API+"Cart/User/Checkout",checkout,this.httpOptions)
  }

  getAllAccount(){
    return this.httpClient.get(this.REST_API+"Account/get/All",this.httpOptions)
  }
}
