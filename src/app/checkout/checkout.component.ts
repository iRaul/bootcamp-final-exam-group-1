import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CartService } from '../services/cart.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators'; 
import Swal from 'sweetalert2'
import { AccountService } from '../services/account.service';
import { Account } from '../model/account';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  constructor(
    private router: Router,
    private cartService:CartService,
    private accountService:AccountService,
       private formBuilder: FormBuilder,
  ) { }
  index:number=0;
  mainBankAcc:any[]=[]
  bankAccounts:any[]=[]
  bankNames:string[]=[]
  bankAccountID:any[]=[]
  submitted:boolean=false;
  fullName:any;
  mobileNumber:any;
  address:any;
  userID:any;
  carts:any[]=[]
  totalAmount:any;
  totalItem:any;
  toggleBank:boolean = true;
  option:string='COD';
  toggleCOD:boolean = true;
  showBank:boolean=false;
  showAccInfo:boolean=false;
  //SHOULD BE ARRAY DAPAT TO E... FOR TESTING LNAG MUNA KAYA NAKA ANY..
  months = ['January', 'Februaru', 'March','April','May','June','July','August','September','October','November','December']
  years=['2020','2021','2022','2023','2024','2025','2026','2027','2028','2029','2030']
  showSaveAccount:boolean=false;
  username:any;
  showBtnAdd=false
  form: FormGroup;
  accountForm = this.formBuilder.group({
    accountName: '',
    accountType: '',
    balance: '',
    accountNumber:'',
    expirationDate:'',
    user_id:''
  });
  

  ngOnInit(): void {
    
    Swal.fire({ 
      background:'transparent',
      timer: 1000
    });
    Swal.showLoading()
    this.fullName = localStorage.getItem('full_name')
    this.address = localStorage.getItem('address')
    this.mobileNumber = localStorage.getItem('mobile_number')
    this.userID = localStorage.getItem('userid');
    this.totalAmount=localStorage.getItem('checkout-total-amount')
    this.totalItem=localStorage.getItem('checkout-total-item')

    this.cartService.getCart(this.userID).pipe(first()).subscribe((data:any=[])=>{
      this.carts=data.Data.Cart

    })
    this.cartService.getAllAccount().pipe(first()).subscribe((data:any=[])=>{
      this.mainBankAcc=data
      for(let x = 0 ; x < data.length ; x++){
        if(data[x].user.id == this.userID){
            this.bankAccounts.push(data[x])
            this.bankNames.push(data[x].accountName)
            this.bankAccountID.push(data[x].id)
          
        }
      
      }
    })
  
  }

  goToHome(){
    this.router.navigate(['home']).then(()=>{
      window.location.reload()
    })
  }
  logout(){
    Swal.fire({ 
      icon: 'success',  
      title: 'OL Shoppee',  
      text: 'Please Wait..',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      localStorage.clear();
      this.router.navigate(['']).then(() => {
        window.location.reload();
      });
  });
  }
  goToProfile(){
    localStorage.setItem('cart-checkout',"false")
    localStorage.removeItem('checkout-cart-id')
    localStorage.removeItem('checkout-total-item')
    localStorage.removeItem('checkout-total-amount')
    localStorage.removeItem('cart-insert-again')
    localStorage.removeItem('checkout')
    Swal.fire({ 
      background:'transparent',
      timer: 500
    }).then(()=>{
      this.router.navigate(['user/profile']).then(()=>{
        window.location.reload()
      })
    });
    Swal.showLoading()
  
  } 

  computePerRow(num:string,num2:string){
    return Math.round(parseInt(num)*parseInt(num2));
  }

  showAddForm(){
    this.showSaveAccount=true
    this.showAccInfo=false;
  }
  bank() {
    if(this.toggleBank){
      this.toggleBank = !this.toggleBank;
      this.toggleCOD=false;
   
      this.option='Bank'
      this.showBank=true
      this.showBtnAdd=true
      this.showAccInfo=false;
    }
  }
  cod() { 
    if(!this.toggleCOD){
      this.toggleCOD = !this.toggleCOD;
      this.toggleBank=true;
      this.showSaveAccount =false;
      this.option='COD'
      this.showBank=false
      this.showBtnAdd=false
      this.showAccInfo=false;
    }
  }
  getBankAccountDetails(val:string){
    this.bankAccounts = this.mainBankAcc.filter(res=>{
      return res.accountName.toLocaleLowerCase().match(val.toLocaleLowerCase())
    })
    this.showSaveAccount=false
    this.showAccInfo=true;
    console.log(this.bankAccounts)
  }
  placeOrder(){
    console.log(this.option)
    if(this.option=='COD'){
      this.cartService.checkoutCOD(this.userID).pipe(first()).subscribe(()=>{
        Swal.fire({ 
          icon: 'success',  
          title: 'Digital Express',  
          text: 'Successfull checkout',
          showConfirmButton: false,  
          timer: 1000
        }).then(()=>{
          this.router.navigate(['home']).then(() => {
            window.location.reload();
          });
        })
      })
    } else {
      if(parseInt(this.totalAmount) > parseInt(this.bankAccounts[0].balance)){
        Swal.fire({ 
          icon: 'error',  
          title: 'Digital Express',  
          text: 'Your account has insufficient funds to cover you total amount. Please try others account!.',
          showConfirmButton: false,  
          timer: 3000
        })
      } else {
        
        this.cartService.checkout(this.userID,this.bankAccounts[0].id).pipe(first()).subscribe((data:any)=>{
          Swal.fire({ 
            icon: 'success',  
            title: 'Digital Express',  
            text: 'Successfull checkout',
            showConfirmButton: false,  
            timer: 1000
          }).then(()=>{
            this.router.navigate(['home']).then(() => {
              window.location.reload();
            });
          })
        })
      }
    }
  }
  get f() { return this.accountForm.controls; }
  onSubmit(){
    this.submitted=true
    
    if(this.accountForm.invalid || this.f.accountName.errors || this.f.accountType.errors || this.f.accountNumber.errors || this.f.balance.errors){
      return;
    }else {
      this.accountForm.patchValue({user_id:this.userID})
    this.accountForm.patchValue({expirationDate:"2018-11-30T18:35:24.00Z"})
      console.log(this.accountForm.value)
      this.accountService.addAccount(this.accountForm.value).pipe(first()).subscribe((data:any=[])=>{
        console.log(this.bankAccountID)
        Swal.fire({ 
          icon: 'success',  
          title: 'Digital Express',  
          text: 'User Link Account Successfull!.',
          showConfirmButton: false,  
          timer: 1500
        }).then(()=>{
          window.location.reload();
        })
      })
    }
   
  }
}
