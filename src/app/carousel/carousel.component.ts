import { animate,style,trigger,transition } from '@angular/animations';
import { Component, OnInit,Input } from '@angular/core';
import { ImagePath } from '../model/image-path';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  // animations:[
  //   trigger('carouselAnimation',[
  //       transition('void=>*',[
  //         style({opacity:0}),
  //         animate('2s',style({opacity:1}))
  //       ])
  //   ])
  // ]

})
export class CarouselComponent implements OnInit {
  @Input() slides:ImagePath[];
  currentIndex:number=0;
  counterValue="10%"
  constructor() { }
  
  ngOnInit(): void {

  }
  next(){
    if(this.currentIndex<this.slides.length-1){
      this.currentIndex++;
    }else{
      this.currentIndex=0;
    }
  }
  prev(){
    if(this.currentIndex>0){
      this.currentIndex--;
    }else{
      this.currentIndex=this.slides.length-1;
    }

  }

}
