import { NgModule,  CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CategoryComponent } from './category/category.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductComponent } from './product/product.component';
import { ProductViewComponent } from './product/product-view/product-view.component';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';
import { CreditCardComponent } from './credit-card/credit-card.component';
import { HomeComponent } from './home/home.component';
import { PaginationComponent } from './pagination/pagination.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { HomeProductComponent } from './home/home-product/home-product.component';
import { HomePaginationComponent } from './home/home-pagination/home-pagination.component';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotFoundComponent } from './not-found/not-found.component';
import { CategoryViewComponent } from './product/category-view/category-view.component';
import { BrandViewComponent } from './product/brand-view/brand-view.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProfileComponent } from './profile/profile.component';
import { AccountComponent } from './profile/account/account.component';
import { TransactionComponent } from './profile/transaction/transaction.component';
@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    FooterComponent,
    CarouselComponent,
    CategoryComponent,
    ProductComponent,
    ProductViewComponent,
    CartComponent,
    CreditCardComponent,
    HomeComponent,
    PaginationComponent,
    HomeProductComponent,
    HomePaginationComponent,
    NotFoundComponent,
    CategoryViewComponent,
    BrandViewComponent,
    CheckoutComponent,
    ProfileComponent,
    AccountComponent,
    TransactionComponent
 
  ],
  imports: [
    SimpleNotificationsModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPaginationModule,
  
  ],
  providers: [HeaderComponent],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
})
export class AppModule { }
