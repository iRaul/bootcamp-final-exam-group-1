import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { User } from 'src/app/model/user';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private REST_API="http://45.130.229.44:8080/api/1.0.0/";
  //  private REST_API="http://localhost:8080/api/1.0.0/";
  prodID:any;
  public user: Observable<User>;

  constructor(private httpClient: HttpClient, private route: Router) { }

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json',
		'Accept': 'application/json',
	'Authorization' : 'Basic ' + btoa('admin:admin'),
	'Access-Control-Allow-Headers' : 'X-Requested-With,content-type'})};
  
  login(user: User) {
    return this.httpClient.post(this.REST_API + "User/Login",user, this.httpOptions);
  } 
  register(user: any){
    return this.httpClient.post(this.REST_API + "User/Customer/Register",user, this.httpOptions);
  }

  //PRODUCTS API 
  getAllProduct(){ //GET 10 PRODUCTS WITH PAGINATION...
    return this.httpClient.get(this.REST_API+"Product/All/1",this.httpOptions);
  }
  //THIS API IS NOT USED
  getProductById(prodID:any){
    this.prodID=prodID;
    return this.httpClient.get(this.REST_API+`Product/${prodID}`, this.httpOptions);
  }

  getOrder(user_id:any){
    const order = {
      user_id:user_id,
      status:"Checkout"
    }
    return this.httpClient.post(this.REST_API+"Order",order,this.httpOptions)
  }

}
