import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/data.service';
import { HeaderComponent } from '../../app/header/header.component'
import { FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { LocationStrategy,PlatformLocation } from '@angular/common';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  status:any;
  isLogin=false;
  loading=false;
  notMatched = false  ;
  submitted = false;
  hide = true;
  username_session:String="";
  id_session:String="";
  imagePath = "../../assets/img/off-eye.png";
  loginForm = this.formBuilder.group({
    username: '',
    password: ''
  });
  constructor(
    private router: Router,
    private dataService: DataService,
    private formBuilder: FormBuilder,
  
  ) { }
  ngOnInit(): void {
    localStorage.clear()
    localStorage.setItem("cart-checkout","false")
    Swal.fire({ 
      showConfirmButton: false,
      background: 'transparent',  
      timer: 200
    });
    Swal.showLoading()
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.status = localStorage.getItem('status');
    if(status === "active"){
      this.router.navigate(['product/view']);
    }
  }

  showPassword (){
    if(this.hide){  
      this.imagePath="../../assets/img/on-eye.png";
      this.hide=false;
    
    } else {
      this.imagePath="../../assets/img/off-eye.png";

      this.hide=true;
      
    }
  }
  get f() { 
    return this.loginForm.controls; 
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }
    console.log(this.loginForm.value+"LOGIN FORM")
    this.dataService.login(this.loginForm.value).pipe(first()).subscribe((data:any) => {
      if(!data){
      
          Swal.fire({ 
             
            icon: 'error',  
            title: 'OL Shoppee',  
            text: 'The username or password you entered is incorrect.?',
            showConfirmButton: false,  
            timer: 2500,
          }).then(()=>{
            window.location.reload();
          })
          this.notMatched = true;
       
        
      }else{
        localStorage.clear();
        Swal.fire({ 
          icon: 'success',  
          title: 'OL Shoppee',  
          text: 'Successfull Login..',
          showConfirmButton: false,  
          timer: 1500
        }).then(()=>{
          this.notMatched=false;
          localStorage.setItem('userid',data['id']);
          localStorage.setItem('full_name',data['name'])
          localStorage.setItem('username',data['username']);
          localStorage.setItem('address',data['address'])
          localStorage.setItem('mobile_number',data['mobileNumber'])
          localStorage.setItem('loginStatus','active');
          this.router.navigate(['home']).then(() => {
            window.location.reload();
          });
        })
        
      }
    },error =>{
      this.loading=false;
    });
    
    // this.f.username.setErrors({notMatched: true});
    // this.f.password.setErrors({notMatched: true});

  }
}
