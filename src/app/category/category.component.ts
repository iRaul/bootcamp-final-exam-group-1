import { Component, OnInit,Input } from '@angular/core';
import { ImagePath } from '../model/image-path';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  @Input() categories:ImagePath[];

  constructor(
    private router:Router
  ) { }
  ngOnInit(): void {
   
  } 
  goToCategory(id:any,name:string){
    Swal.fire({  
      title: 'OL Shoppee',  
      text: 'One Moment Please...',
      showConfirmButton: false,  
      timer: 1000
    }).then(()=>{
      this.router.navigate(['product/category/'+name+'/'+id]).then(() => {
        localStorage.setItem('menu-title','Category')
        localStorage.setItem('menu-name',name);
        localStorage.setItem('menu-page',id)
        window.location.reload();
      });
    })
    Swal.showLoading()
  }  
  

}
