import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { first } from 'rxjs/operators'
import { Cart } from '../model/cart'
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  qty:number =0;
  carts:any[]=[];
  userID:any;
  cartLength:any;
  total:any;
  isChecked:boolean[]=[];
  eachPrice:number[]=[];
  page:number=1;
  productName:string=""
  totalItem:number=0;
  totalAmount:number=0;
  showTotalPrice:boolean=false;

  cart_id:number[]=[];
  showBtnDelete:boolean=false;

  checkOut:any[]=[];
  delProdID:number[]=[];
  constructor(
    private router:Router,
    private cartService: CartService,
    ) { }

  ngOnInit(): void {
    localStorage.setItem('cart-checkout',"false")
    localStorage.removeItem('menu-title')
    localStorage.removeItem('menu-brand')
    localStorage.removeItem('menu-category')
    localStorage.removeItem('menu-prod-name')
    localStorage.removeItem('prod-sorting')
    localStorage.removeItem("prod-id")
    localStorage.removeItem('checkout-total-item')
    localStorage.removeItem('checkout-total-amount')
    localStorage.removeItem('checkout')
    localStorage.removeItem('checkout-cart-id')
    
    this.userID=localStorage.getItem("userid")
    this.cartService.getCart(this.userID).pipe(first()).subscribe((data:any=[])=>{
      this.carts=data.Data.Cart;
      this.cartLength=this.carts.length ;
      for(let x = 0 ; x < this.cartLength ; x++){
        this.delProdID.push(parseInt(this.carts[x].id))
      }
    })
  }
  checkCheckBoxvalue(event:any,cartID:any,id:any){
    if(event.target.checked){
      this.isChecked[id]=true;
      this.totalItem++;
      this.cart_id.push(parseInt(cartID))
      if(this.eachPrice.indexOf(parseInt(event.target.value),0) == -1){
        this.eachPrice.push(parseInt(event.target.value))
        this.checkOut.push(this.carts[id])
        this.delProdID.splice(this.delProdID.indexOf(cartID,0),1)
      }
      
    } else {
      this.isChecked[id]=false;
      this.totalItem--;
      this.delProdID.push(parseInt(cartID))
      this.eachPrice.splice(this.eachPrice.indexOf(event.target.value,0),1)
      this.checkOut.splice(this.checkOut.indexOf(this.carts[id],0),1)
      this.cart_id.splice(this.cart_id.indexOf(cartID,0),1)
    }
    if(this.totalItem>0){
      this.showTotalPrice=true;
      this.showBtnDelete=true
    } else {
      this.showTotalPrice=false;
      this.showBtnDelete=false
    }
    console.log(this.delProdID,"DELETE PRODUCT")
    console.log(this.checkOut,"CHECKOUT PRODUCT")
    this.totalAmount = this.eachPrice.reduce((acc,cur)=>acc+cur,0)
 }
  computePerRow(num:string,num2:string){
    return Math.round(parseInt(num)*parseInt(num2));
  }
  checkout(){
    if(this.totalItem>0){
 
      this.cartService.deleteCart(this.delProdID).pipe(first()).subscribe(data=>{
        this.router.navigate(['product/cart/checkout']).then(()=>{
          localStorage.setItem('cart-checkout',"true")
          localStorage.setItem('checkout-cart-id',JSON.stringify(this.cart_id))
          localStorage.setItem('checkout-total-item',this.totalItem.toString())
          localStorage.setItem('checkout-total-amount',this.totalAmount.toString())
          localStorage.setItem('cart-insert-again',JSON.stringify(this.delProdID))
          localStorage.setItem('checkout', JSON.stringify(this.checkOut));
          window.location.reload();
        })
      })
     
    }else{
      alert("walang selcted")
    }
   
  }
  
  addQty(index:number,cartID:any,amountRow:any){
    if(parseInt(this.carts[index].orderQuantity) ==  parseInt(this.carts[index].product.productQuantity)){
    }else{
        this.carts[index].orderQuantity = ((+this.carts[index].orderQuantity)+1).toString();
        if(this.eachPrice.indexOf(amountRow,0) == -1){
          this.eachPrice.push(parseInt(amountRow)+parseInt(this.carts[index].product.price))
        } else {
          this.eachPrice[this.eachPrice.indexOf(amountRow,0)]=parseInt(amountRow)+parseInt(this.carts[index].product.price)
        }
      this.cartService.updateCart(cartID,this.carts[index].orderQuantity).subscribe()
    }

   
    if(this.isChecked[index]){
      this.totalAmount = this.eachPrice.reduce((acc,cur)=>acc+cur,0)
    }
  }
  minusQty(index:number,cartID:any,amountRow:any){
    if(parseInt(this.carts[index].orderQuantity) == 0){
    }else{
      this.carts[index].orderQuantity = ((+this.carts[index].orderQuantity)-1).toString();
      
      if(this.eachPrice.indexOf(amountRow,0) == -1){
        this.eachPrice.push(parseInt(amountRow)-parseInt(this.carts[index].product.price))
      } else {
        this.eachPrice[this.eachPrice.indexOf(amountRow,0)]=parseInt(amountRow)-parseInt(this.carts[index].product.price)
      }
      this.cartService.updateCart(cartID,this.carts[index].orderQuantity).subscribe()
    }
    if(this.isChecked[index]){
      this.totalAmount = this.eachPrice.reduce((acc,cur)=>acc+cur,0)
    }
  }

  searchProduct(){
    if(this.productName == ""){
      this.totalAmount=0;
      this.ngOnInit();
    }else{
      this.carts = this.carts.filter(res => {
        if(res.product.length != 0){
          return res.product.productName.toLocaleLowerCase().match(this.productName.toLocaleLowerCase())
        } else {
          this.totalAmount=0
          this.totalAmount
        }
       
      })  
    }
  }

  deleteCart(){
    if(this.totalItem>0){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Deleted!',
            'success'
          ).then(()=>{
            localStorage.removeItem("prod-id")
            window.location.reload(); 
          })
          this.cartService.deleteCart(this.cart_id).pipe(first()).subscribe(data=>{
          })
        } 
      })
      
    } else{
      alert("INVALID DELETE NO PRODUCT SELECTED")
    }
  }
}
