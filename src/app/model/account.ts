export class Account {
    accountName:string;
    accountNumber:string;
    accountType:string;
    balance:string;
    expirationDate:string;
    user_id:string;
    constructor(accountName:string,accountNumber:string,accountType:string,balance:string,expirationDate:string,user_id:string)
    {
        this.accountName=accountName
        this.accountNumber=accountNumber
        this.accountType=accountType
        this.balance=balance
        this.expirationDate=expirationDate
        this.user_id=user_id
    }
}