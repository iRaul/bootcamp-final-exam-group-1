export class AddCart{
    user_id:number;
    product_id:number;
    order_quantity:number;
    
    constructor(user_id:number,product_id:number,order_quantity:number){
        this.user_id=user_id;
        this.product_id=product_id;
        this.order_quantity=order_quantity
    }
}