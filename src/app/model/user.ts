export interface User {
    id:Number;
    username: String;
    name: String;
    password: String;
    type: String;
    email:String;
    address:String;
    mobileNumber:String;
}
