export class Cart {
    product: Object;
    orderQuantity:number;   
    id: number;
    constructor(product:Object,orderQuantity:number,id:number){
        this.product=product;
        this.orderQuantity=orderQuantity;
        this.id=id;
    }
  }