export class Product{
    id:string;
    productName:string;
    category:string;
    brand:string;
    productDescription:string;
    imagePath:string;
    productQuantity:string;
    price:string;
    constructor(id:string, productName:string,category:string,brand:string,productDescription:string,imagePath:string,productQuantity:string,price:string){
        this.id=id;
        this.productName=productName;
        this.category=category;
        this.brand=brand;
        this.productDescription=productDescription;
        this.imagePath=imagePath;
        this.productQuantity=productQuantity;
        this.price=price;
    }
}